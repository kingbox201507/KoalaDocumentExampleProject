
package com.xiaokaceng.demo.application.impl.core;

import java.util.List;
import java.util.ArrayList;
import java.text.MessageFormat;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;
import org.dayatang.domain.InstanceFactory;
import org.dayatang.querychannel.Page;
import org.dayatang.querychannel.QueryChannelService;
import com.xiaokaceng.demo.application.dto.*;
import com.xiaokaceng.demo.application.core.BookApplication;
import com.xiaokaceng.demo.core.*;

@Named
@Transactional
public class BookApplicationImpl implements BookApplication {


	private QueryChannelService queryChannel;

    private QueryChannelService getQueryChannelService(){
       if(queryChannel==null){
          queryChannel = InstanceFactory.getInstance(QueryChannelService.class,"queryChannel");
       }
     return queryChannel;
    }
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public BookDTO getBook(Long id) {
		Book book = Book.load(Book.class, id);
		BookDTO bookDTO = new BookDTO();
		// 将domain转成VO
		try {
			BeanUtils.copyProperties(bookDTO, book);
		} catch (Exception e) {
			e.printStackTrace();
		}
		bookDTO.setId((java.lang.Long)book.getId());
		return bookDTO;
	}
	
	public BookDTO saveBook(BookDTO bookDTO) {
		Book book = new Book();
		try {
        	BeanUtils.copyProperties(book, bookDTO);
        } catch (Exception e) {
        	e.printStackTrace();
        }
		book.save();
		bookDTO.setId((java.lang.Long)book.getId());
		return bookDTO;
	}
	
	public void updateBook(BookDTO bookDTO) {
		Book book = Book.get(Book.class, bookDTO.getId());
		// 设置要更新的值
		try {
			BeanUtils.copyProperties(book, bookDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void removeBook(Long id) {
		this.removeBooks(new Long[] { id });
	}
	
	public void removeBooks(Long[] ids) {
		for (int i = 0; i < ids.length; i++) {
			Book book = Book.load(Book.class, ids[i]);
			book.remove();
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<BookDTO> findAllBook() {
		List<BookDTO> list = new ArrayList<BookDTO>();
		List<Book> all = Book.findAll(Book.class);
		for (Book book : all) {
			BookDTO bookDTO = new BookDTO();
			// 将domain转成VO
			try {
				BeanUtils.copyProperties(bookDTO, book);
			} catch (Exception e) {
				e.printStackTrace();
			}
			list.add(bookDTO);
		}
		return list;
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Page<BookDTO> pageQueryBook(BookDTO queryVo, int currentPage, int pageSize) {
		List<BookDTO> result = new ArrayList<BookDTO>();
		List<Object> conditionVals = new ArrayList<Object>();
	   	StringBuilder jpql = new StringBuilder("select _book from Book _book   where 1=1 ");
	
	
	   	if (queryVo.getName() != null && !"".equals(queryVo.getName())) {
	   		jpql.append(" and _book.name like ?");
	   		conditionVals.add(MessageFormat.format("%{0}%", queryVo.getName()));
	   	}		
	
	   	if (queryVo.getTitle() != null && !"".equals(queryVo.getTitle())) {
	   		jpql.append(" and _book.title like ?");
	   		conditionVals.add(MessageFormat.format("%{0}%", queryVo.getTitle()));
	   	}		
        Page<Book> pages = getQueryChannelService().createJpqlQuery(jpql.toString()).setParameters(conditionVals).setPage(currentPage, pageSize).pagedList();
        for (Book book : pages.getData()) {
            BookDTO bookDTO = new BookDTO();
            
             // 将domain转成VO
            try {
            	BeanUtils.copyProperties(bookDTO, book);
            } catch (Exception e) {
            	e.printStackTrace();
            } 
            
                                                result.add(bookDTO);
        }
        return new Page<BookDTO>(pages.getPageIndex(), pages.getResultCount(), pageSize, result);
	}
	
	
}
