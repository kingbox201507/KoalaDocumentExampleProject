package org.openkoala.mybatisdemo.facade.impl;

import java.util.Date;

import javax.inject.Inject;

import org.dayatang.querychannel.Page;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openkoala.koala.util.KoalaBaseSpringTestCase;
import org.openkoala.mybatisdemo.core.domain.Employee;
import org.openkoala.mybatisdemo.facade.EmployeeFacade;

public class EmployeeApplicationTest extends KoalaBaseSpringTestCase { 
	 
 	@Inject
 	private EmployeeFacade employeeFacade; 
 
 	@Test
    public void test(){ 
    	for(int i=0;i<11;i++){
    		Employee employee = new Employee();
    		employee.setName("lingen");
    		employee.setBirthDate(new Date());
    		employee.setGender("man");
    		employee.setAge(i);
    		employee.save();
    	}
    	Page<Employee> pages = employeeFacade.findEmployeeByName("lingen", 1, 10); 
    	assertTrue(pages.getPageCount()==2);
    	assertTrue(pages.getStart()==10);
    	assertTrue(pages.getData().size()==1);
    } 
 }

