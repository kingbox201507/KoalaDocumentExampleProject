package org.openkoala.mybatisdemo.application.impl;

import java.util.List;

import javax.inject.Named;

import org.openkoala.mybatisdemo.application.EmployeeApplication;
import org.openkoala.mybatisdemo.core.domain.Employee;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Named
@Transactional
public class EmployeeApplicationImpl implements EmployeeApplication {

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Employee getEmployee(Long id) {
		return Employee.getRepository().get(Employee.class, id);
	}
	
	public Employee saveEmployee(Employee employee) {
		employee.save();
		return employee;
	}
	
	public void updateEmployee(Employee employee) {
		employee .save();
	}
	
	public void removeEmployee(Long  id) {
		this.removeEmployees(new Long[] { id });
	}
	
	public void removeEmployees(Long[] ids) {
		for (int i = 0; i < ids.length; i++) {
			Employee employee = Employee.getRepository().get(Employee.class, ids[i]);
			employee.remove();
		}
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Employee> findAllEmployee() {
		return Employee.getRepository().findAll(Employee.class);
	}

}
