package org.openkoala.demo.core.domain;

import static org.junit.Assert.*;
import org.junit.Test;
import org.openkoala.koala.util.KoalaBaseSpringTestCase;
public class EmployeeTest extends KoalaBaseSpringTestCase {
	@Test
	public void test() {
		Employee employee = new Employee();
		employee.setName("张三");
		employee.setAge(18);
		employee.save();
		
		Organization organization = new Organization();
		organization.setName("foreveross");
		organization.setSerialNumber("222");
		organization.save();
		
		assertEquals("张三", Employee.get(Employee.class, employee.getId()).getName());
		assertTrue(Employee.findByAgeRange(0, 20).contains(employee));
		
		employee.assignTo(organization);
		assertEquals(organization, Employee.get(Employee.class, employee.getId()).getOrganization());
		
		employee.setAge(24);
		employee.save();
		assertEquals(24, Employee.get(Employee.class, employee.getId()).getAge());
		assertTrue(Employee.findByAgeRange(20, 30).contains(employee));
		
		
		employee.remove();
		organization.remove();
	}
}
