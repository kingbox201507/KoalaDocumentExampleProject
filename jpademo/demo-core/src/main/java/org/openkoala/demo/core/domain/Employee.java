package org.openkoala.demo.core.domain;
 
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.openkoala.koala.commons.domain.KoalaAbstractEntity;
 
@Entity
@Table(name = "employees")
public class Employee extends KoalaAbstractEntity {
 
    /**
     * 
     */
    private static final long serialVersionUID = -2700241427681303048L;
 
    private String name;
     
    private int age;
     
    private String gender;
     
   private Organization organization;
     
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public int getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
 
    public String getGender() {
        return gender;
    }
 
    public void setGender(String gender) {
        this.gender = gender;
    }
 
    @ManyToOne
    @JoinColumn(name = "organization_id")
    public Organization getOrganization() {
        return organization;
    }
 
    public void setOrganization(Organization organization) {
        this.organization = organization;
    }
    
    public void assignTo(Organization organization) {
    	this.setOrganization(organization);
    	this.save();
    }
    
    public static List<Employee> findByAgeRange(Integer from, Integer to) {
    	return getRepository().createNamedQuery("findEmployeesByAgeRange").setParameters(new Object[]{from, to}).list();
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Employee other = (Employee) obj;
        if (getId() == null) {
            if (other.getId() != null)
                return false;
        } else if (!getId().equals(other.getId()))
            return false;
        return true;
    }
 
    public String toString() {
        return name;
    }

	@Override
	public String[] businessKeys() {
		return null;
	}
     
}
