<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="jumbotron">
	    <h3>欢迎登录 Koala</h3>
	    <p>访问<a href="http://openkoala.org" target="_blank">Koala主页</a>，获取最新资讯与信息</p>
	    <p>如何使用Koala，查看<a href="http://wiki.openkoala.org/display/KD3/Koala+DOC+3+Home" target="_blank">在线使用指南</a></p>
	    <p>提交BUG或改进意义，请访问<a href="http://jira.openkoala.org/issues/?filter=-4" target="_blank">BUG提交</a></p>
	</div>
</body>
</html>