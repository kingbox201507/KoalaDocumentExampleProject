package org.openkoala.security.taglibs;

import java.util.Collection;

import javax.servlet.ServletContext;

import org.openkoala.security.facade.SecurityConfigFacade;
import org.openkoala.security.shiro.CurrentUser;
import org.openkoala.security.shiro.realm.ShiroUser;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class AuthzImpl implements Authz {

	private ApplicationContext applicationContext;

	private SecurityConfigFacade securityConfigFacade;

	private ServletContext servletContext;

	public ApplicationContext getApplicationContext() {
		if (applicationContext == null) {
			applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		}
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	
	public Object getPrincipal() {
		return CurrentUser.getPrincipal();
	}

	
	public boolean ifAllRole(Collection<String> roles) {
		return CurrentUser.getSubject().hasAllRoles(roles);
	}

	
	public boolean ifAnyRole(Collection<String> roles) {
		for (String role : roles) {
			if (CurrentUser.getSubject().hasRole(role)) {
				return true;
			}
		}
		return false;
	}

	
	public boolean ifNotRole(Collection<String> roles) {
		return !ifAnyRole(roles);
	}

	
	public boolean ifAllPermission(Collection<String> permissions) {
		return CurrentUser.getSubject().isPermittedAll(permissions.toString());
	}

	
	public boolean ifAnyPermission(Collection<String> permissions) {
		for (String permission : permissions) {
			if (CurrentUser.getSubject().isPermitted(permission)) {
				return true;
			}
		}
		return false;
	}

	
	public boolean ifNotPermission(Collection<String> permissions) {
		return !ifAnyPermission(permissions);
	}

	
	public boolean hasSecurityResource(String securityResourceIdentifier) {
		ShiroUser shiroUser = (ShiroUser) getPrincipal();
		if (shiroUser == null) {
			return false;
		}
		
		String userAccount = shiroUser.getUserAccount();
		String currentRoleName = shiroUser.getRoleName();
		
		if(securityConfigFacade == null){
			securityConfigFacade = getApplicationContext().getBean(SecurityConfigFacade.class);
		}
		
		return securityConfigFacade.checkUserHasPageElementResource(userAccount, currentRoleName,securityResourceIdentifier);
		
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}
