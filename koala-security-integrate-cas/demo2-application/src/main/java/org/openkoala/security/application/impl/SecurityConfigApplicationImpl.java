package org.openkoala.security.application.impl;

import java.util.List;
import java.util.Set;

import javax.inject.Named;
import org.springframework.transaction.annotation.Transactional;

import org.openkoala.security.application.SecurityConfigApplication;
import org.openkoala.security.core.domain.Actor;
import org.openkoala.security.core.domain.Authority;
import org.openkoala.security.core.domain.MenuResource;
import org.openkoala.security.core.domain.PageElementResource;
import org.openkoala.security.core.domain.Permission;
import org.openkoala.security.core.domain.Role;
import org.openkoala.security.core.domain.Scope;
import org.openkoala.security.core.domain.SecurityResource;
import org.openkoala.security.core.domain.UrlAccessResource;
import org.openkoala.security.core.domain.User;
import com.google.common.collect.Sets;

@Named
@Transactional(value = "transactionManager_security")
public class SecurityConfigApplicationImpl implements SecurityConfigApplication {

    public void createActor(Actor actor) {
        actor.save();
    }

    public void terminateActor(Actor actor) {
        actor.remove();
    }

    public void suspendUser(User user) {
        user.disable();
    }

    public void activateUser(User user) {
        user.enable();
    }

    public void createAuthority(Authority authority) {
        authority.save();
    }

    public void terminateAuthority(Authority authority) {
        authority.remove();
    }

    public void createSecurityResource(SecurityResource securityResource) {
        securityResource.save();
    }

    public void terminateSecurityResource(SecurityResource securityResource) {
        securityResource.remove();
    }

    public void grantRoleToPermission(Role role, Permission permission) {
        role.addPermission(permission);
    }

    public void grantRoleToPermissions(Role role, List<Permission> permissions) {
        role.addPermissions(permissions);
    }

    public void grantRolesToPermission(List<Role> roles, Permission permission) {
        for (Role role : roles) {
            role.addPermission(permission);
        }
    }

    public void grantActorToAuthority(Actor actor, Authority authority) {
        actor.grant(authority, null);
    }

    public void grantActorToAuthorities(Actor actor, List<Authority> authorities) {
        for (Authority authority : authorities) {
            this.grantActorToAuthority(actor, authority);
        }
    }

    public void grantActorsToAuthority(List<Actor> actors, Authority authority) {
        for (Actor actor : actors) {
            this.grantActorToAuthority(actor, authority);
        }
    }

    public void terminateSecurityResourceFromAuthority(SecurityResource securityResource, Authority authority) {
        authority.terminateSecurityResource(securityResource);
    }

    public void terminateSecurityResourcesFromAuthority(List<? extends SecurityResource> securityResources, Authority authority) {
        authority.terminateSecurityResources(Sets.newHashSet(securityResources));
    }

    public void terminateAuthoritiesFromSecurityResource(List<Authority> authorities, SecurityResource securityResource) {
        for (Authority authority : authorities) {
            terminateAuthorityFromSecurityResource(authority, securityResource);
        }
    }

    public void terminateAuthorityFromSecurityResource(Authority authority, SecurityResource securityResource) {
        authority.terminateSecurityResource(securityResource);
    }

    public void terminatePermissionFromRole(Permission permission, Role role) {
        role.terminatePermission(permission);
    }

    public void terminatePermissionsFromRole(List<Permission> permissions, Role role) {
        role.terminatePermissions(permissions);
    }

    public void terminateRolesFromPermission(List<Role> roles, Permission permission) {
        for (Role role : roles) {
            terminateRoleFromPermission(role, permission);
        }
    }

    private void terminateRoleFromPermission(Role role, Permission permission) {
        role.terminatePermission(permission);
    }

    public void terminateActorFromAuthority(Actor actor, Authority authority) {
        actor.terminate(authority);
    }

    public void terminateAuthoritiesFromActor(List<Authority> authorities, Actor actor) {
        for (Authority authority : authorities) {
            this.terminateActorFromAuthority(actor, authority);
        }
    }

    public void createScope(Scope scope) {
        scope.save();
    }

    public void grantActorToAuthorityInScope(Actor actor, Authority authority, Scope scope) {
        actor.grant(authority, scope);
    }

    
    public void grantAuthorityToSecurityResource(Authority authority, SecurityResource securityResource) {
        authority.addSecurityResource(securityResource);

    }

    
    public void grantAuthorityToSecurityResources(Authority authority, List<? extends SecurityResource> securityResources) {
        authority.addSecurityResources(securityResources);
    }

    
    public void resetPassword(User user) {
        user.resetPassword();
    }

    
    public void createChildToParent(MenuResource child, Long parentId) {
        MenuResource parent = MenuResource.get(MenuResource.class, parentId);
        parent.addChild(child);
    }

    
    public void grantSecurityResourcesToAuthority(List<? extends SecurityResource> securityResources, Authority authority) {
        authority.addSecurityResources(securityResources);
    }

    
    public void grantPermissionToRole(Permission permission, Role role) {
        role.addPermission(permission);
    }

    
    public void grantSecurityResourceToAuthority(SecurityResource securityResource, Authority authority) {
        authority.addSecurityResource(securityResource);
    }

    
    public void grantPermissionsToRole(List<Permission> permissions, Role role) {
        role.addPermissions(permissions);
    }

    
    public boolean checkAuthoritiHasPageElementResource(Set<Authority> authorities, String identifier) {
        return Authority.checkHasPageElementResource(authorities, identifier);
    }

    
    public void grantAuthorityToActor(Authority authority, Actor actor) {
        actor.grant(authority);
    }

    
    public void changeUserAccount(User user, String userAccount, String userPassword) {
        user.changeUserAccount(userAccount, userPassword);
    }

    
    public void changeUserEmail(User user, String email, String userPassword) {
        user.changeEmail(email, userPassword);
    }

    
    public void changeUserTelePhone(User user, String telePhone, String userPassword) {
        user.changeTelePhone(telePhone, userPassword);
    }

    
    public void changeNameOfUrlAccessResource(UrlAccessResource urlAccessResource, String name) {
        urlAccessResource.changeName(name);
    }

    
    public void changeUrlOfUrlAccessResource(UrlAccessResource urlAccessResource, String url) {
        urlAccessResource.changeUrl(url);
    }

    
    public void changeNameOfRole(Role role, String name) {
        role.changeName(name);
    }

    
    public void changeNameOfPermission(Permission permission, String name) {
        permission.changeName(name);
    }

    
    public void changeIdentifierOfPermission(Permission permission, String identifier) {
        permission.changeIdentifier(identifier);
    }

    
    public void changeNameOfPageElementResouce(PageElementResource pageElementResource, String name) {
        pageElementResource.changeName(name);
    }

    
    public void changeIdentifierOfPageElementResouce(PageElementResource pageElementResource, String identifier) {
        pageElementResource.changeIdentifier(identifier);
    }

    
    public void changeNameOfMenuResource(MenuResource menuResource, String name) {
        menuResource.changeName(name);
    }

    
    public void terminateActorFromAuthorityInScope(Actor actor, Authority authority, Scope scope) {
        actor.terminateAuthorityInScope(authority, scope);
    }

    
    public void changeLastModifyTimeOfUser(User user) {
        user.changeLastModifyTime();
    }

}
